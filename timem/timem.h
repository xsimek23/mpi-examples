/**
* Simple class - time measurement
* It allows to store walltime of frequently called function
* and get sum of function wall times
*/

#ifndef TIMEM_H
#define TIMEM_H

#include <omp.h>
#include <map>
#include <iostream>

class Time_m
{
private: 
	std::map <std::string, double> _time_array;

public:
	Time_m() {}

	void store_time(std::string name, double duration)
	{
		if(_time_array.find(name) != _time_array.end())
		{
			_time_array[name] += duration;
		}
		else
		{
			_time_array[name] = duration;
		}

  	return;
	}

	double get_time(std::string name)
	{
		return _time_array[name];
	}

	void show_times()
	{
		std::cout << "\n= = = = = = = = Measured time = = = = = = = =\n";
		std::cout << "Function   |   Time\n";
		for (std::map<std::string, double>::iterator it = _time_array.begin(); it != _time_array.end(); ++it)
		{
			std::cout << it->first << "  =>  " << it->second << '\n';
		}

		return;
	}
};

#endif
