#include "timem.h"

int main(void)
{
	Time_m tm;
	tm.store_time(__FUNCTION__, 2.65);
	tm.store_time("sse", 2.60);
	tm.store_time("sse2", 2.68);
	tm.store_time(__FUNCTION__, 2.0);
	tm.store_time("sse2", 2.68);
	tm.store_time("sse2", 0);

	tm.show_times();

	return 0;
}