/**
* PRL 2015/2016 - Minimum Extraction sort 
* Parallel MPI version (OpenMPI)
*
* @author Dominik Simek <xsimek23@stud.fit.vutbr.cz>
* 
* @date 6.3.2016
*/

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <math.h>

// turn on profile mode (measure time of computation and print it)
//#define PROFILE

const int MASTER      =  0;
const int TAG         =  0;
const int EMPTY_VALUE = -1;
const int EMPTY_NODE  = -2;

int main(int argc, char *argv[])
{
  MPI_Init(&argc, &argv);

  int num_procs        = 0;
  int my_rank          = 0;
  int my_number        = EMPTY_VALUE;
  int total_numbers    = 0;
  int first_leaf_proc  = 0;
  int right_child_rank = 0;
  int left_child_rank  = 0;
  int left_child_num   = EMPTY_VALUE;
  int right_child_num  = EMPTY_VALUE;
  int children_empty   = 0;
  int numbers_sorted   = 0;
  int my_parent_rank   = 0;
  double start_time    = 0.0;
  double end_time      = 0.0;
  MPI_Status status;

  MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  first_leaf_proc = (num_procs + 1) / 2 - 1; 

  if(my_rank == MASTER)
  {
    FILE *f          = NULL;
    int num          = EMPTY_VALUE;
    int leaf_proc_rank = 0;

    if(argc < 2)
    {
      fprintf(stderr, "Error: parameters !\n");
      MPI_Abort(MPI_COMM_WORLD, 1);
    }
    f = fopen(argv[1], "r");
    if(!f)
    {
      fprintf(stderr, "Error: fopen() !\n");
      MPI_Abort(MPI_COMM_WORLD, 1);
    }
    // first leaf processor rank
    leaf_proc_rank = first_leaf_proc;
    // read and distribute numbers to all leaf processors 
    while((num = getc(f)) != EOF)
    {
      printf("%d ", num);
      MPI_Send(&num, 1, MPI_INT, leaf_proc_rank, TAG, MPI_COMM_WORLD);
      // go to next leaf processor
      leaf_proc_rank++;
      total_numbers++;
    }
    printf("\n");
    fclose(f);
    // check if all processors have number,
    // if no, put them some EMPTY_VALUE
    // it is necessary because all processor >= first_leaf_proc will wait for data
    while(leaf_proc_rank < num_procs)
    {
      num = EMPTY_VALUE;
      MPI_Send(&num, 1, MPI_INT, leaf_proc_rank, TAG, MPI_COMM_WORLD);
      // go to next leaf processor
      leaf_proc_rank++;
    }
    #ifdef PROFILE
      start_time = MPI_Wtime();
    #endif
  }

  // each leaf processor (N) must recieve one value
  // (total processors == 2N -1)
  if(my_rank >= first_leaf_proc)
  {
    MPI_Recv(&my_number, 1, MPI_INT, MASTER, TAG, MPI_COMM_WORLD, &status);
  }
  else
  {

  }

  // for i = 1 to 2n + (log n) - 1 do:
  // we do not use for loop, because while loop with simple flag condition
  // doesn't need to know number of iterations (we doesn't need to compute and distribute it
  // at start of algorithm)
  while(!numbers_sorted)
  {
    // non leaf processors
    if(my_rank < first_leaf_proc)
    {
      // if I am master and I have value, print it
      if(my_rank == MASTER && my_number != EMPTY_VALUE)
      {
        printf("%d\n", my_number);
        my_number = EMPTY_VALUE;
      }
      // not leaf node, no valid value
      if(my_number == EMPTY_VALUE && children_empty != EMPTY_NODE)
      {
        // get ranks of childs and recieve values
        left_child_rank  = my_rank * 2 + 1;
        right_child_rank = my_rank * 2 + 2;
        MPI_Recv(&left_child_num, 1, MPI_INT, left_child_rank, TAG, MPI_COMM_WORLD, &status);
        MPI_Recv(&right_child_num, 1, MPI_INT, right_child_rank, TAG, MPI_COMM_WORLD, &status);

        // all children are empty
        if(left_child_num == EMPTY_VALUE && right_child_num == EMPTY_VALUE)
        {
          // nums are now probably sorted, "distribute" this information
          if(my_rank == MASTER)
            numbers_sorted = 1;
          children_empty  = EMPTY_NODE;
          left_child_num  = EMPTY_NODE;
          right_child_num = EMPTY_NODE;
        }
        else
        {
          // take value from from left child
          if(left_child_num != EMPTY_VALUE && right_child_num == EMPTY_VALUE)
          {
            my_number      = left_child_num;
            left_child_num = EMPTY_VALUE;
          }
          // take value from right child
          else if(left_child_num == EMPTY_VALUE && right_child_num != EMPTY_VALUE)
          {
            my_number       = right_child_num;
            right_child_num = EMPTY_VALUE;
          }
          // compare childs
          else
          {
            // left value is smaller or equal (we take most left value)
            if(left_child_num <= right_child_num)
            {
              my_number      = left_child_num;
              left_child_num = EMPTY_VALUE;
            }
            // we take right value
            else
            {
              my_number       = right_child_num;
              right_child_num = EMPTY_VALUE;
            }
          }
        }
        // update child's values
        MPI_Send(&left_child_num, 1, MPI_INT, left_child_rank, TAG, MPI_COMM_WORLD);
        MPI_Send(&right_child_num, 1, MPI_INT, right_child_rank, TAG, MPI_COMM_WORLD);
      }
    }
    // all processors except MASTER (root)
    if(my_rank != MASTER)
    {
      // get rank of my parent and send him value
      my_parent_rank = ((my_rank % 2) == 0) ? (my_rank - 2) / 2 : (my_rank - 1) / 2;
      MPI_Send(&my_number, 1, MPI_INT, my_parent_rank, TAG, MPI_COMM_WORLD);
      MPI_Recv(&my_number, 1, MPI_INT, my_parent_rank, TAG, MPI_COMM_WORLD, &status);
      
      if(my_number == EMPTY_NODE)
        numbers_sorted = 1;
    }
  }
  #ifdef PROFILE
    if(my_rank == MASTER)
    {
      end_time = MPI_Wtime();
      printf("Wall time (numbers=%d): %.6f\n", total_numbers, end_time - start_time);
    }
  #endif

  MPI_Finalize();
}
