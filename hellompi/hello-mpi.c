/**
* Simple MPI program - Hello MPI
* Master send message to slave, slave send message back to master,
* master print message (from slave)
*
* @author Dominik Simek
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <mpi.h>

const unsigned it = 500;

int main(int argc, char *argv[])
{
  char p_idstr[128];
  char p_buff[128];
  int numprocs = 0;
  int myid     = 0;

  MPI_Status stat;

  // start MPI program
  MPI_Init(&argc, &argv);

  // # of processors
  MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
  // my ID
  MPI_Comm_rank(MPI_COMM_WORLD, &myid);

  // master
  if(myid == 0)
  {
    printf("MASTER: We have %d processors.\n", numprocs);

    for(int i = 1; i < numprocs; i++)
    {
      sprintf(p_buff, "Hello %d", i);
      MPI_Send(p_buff, sizeof(p_buff), MPI_CHAR, i, 0, MPI_COMM_WORLD);
    }

    for(int i = 1; i < numprocs; i++)
    {
      MPI_Recv(p_buff, sizeof(p_buff), MPI_CHAR, i, 0, MPI_COMM_WORLD, &stat);
      printf("%s\n", p_buff);
    }
  }
  // slave
  else
  {
    char hostname[256];
    int len;
    MPI_Get_processor_name(hostname, &len);
    MPI_Recv(p_buff, sizeof(p_buff), MPI_CHAR, 0, 0, MPI_COMM_WORLD, &stat);

    sprintf(p_idstr, "SLAVE: Hello 0, I am processor %d and I am ready to use! (hostname: %s)", myid, hostname);
    // do some computations
    float var = 1.54455f;
    for(unsigned i = 0; i < it; i++)
    {
      for(unsigned j = 0; j < it; j++)
      {
        var = var * 0.566f * j + i;
      }
    }
    MPI_Send(p_idstr, sizeof(p_idstr), MPI_CHAR, 0, 0, MPI_COMM_WORLD);
  }

  MPI_Finalize();

  return EXIT_SUCCESS;
}
