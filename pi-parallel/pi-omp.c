/**
* Program that demonstrate iterative computation of PI value
* Parallel (OpenMP) version (single process, multiple threads)
*
* @author Dominik Simek
*/

#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#ifndef NUM_STEPS
	const unsigned long NSTEPS = 100000000;
#else
	const unsigned long NSTEPS = NUM_STEPS;
#endif

const unsigned NUM_THREADS = 2;

int main(void)
{
	int num_threads = 0;
	double x        = 0.0;
	double sum      = 0.0;
	double pi       = 0.0;
	double step     = 1.0 / (double)NSTEPS;
	double start_time = 0.0;
	double wall_time  = 0.0;

	// PI = integrate(4.0 / (1+x^2), x=0..1)
	// omp_set_num_threads(NUM_THREADS);
	num_threads = omp_get_max_threads();
	start_time = omp_get_wtime();
	#pragma omp parallel for reduction(+:sum) private(x)
	for(unsigned long i = 1; i < NSTEPS; i++)
	{
		x = (i - 0.5) * step;
		sum += 4.0 / (1.0 + x * x);
	}
	pi = sum * step;
	wall_time = omp_get_wtime() - start_time;

	printf("PI = %f (%lu steps of approxiamtion)\n", pi, NSTEPS);
	//printf("Time of computation: %f s (%f ms)\n", wall_time, wall_time * 1000 );
	printf("Time of computation: %.4f s, used %d threads\n", wall_time, num_threads);

	return EXIT_SUCCESS;
}