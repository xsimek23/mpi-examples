/**
* Program that demonstrate iterative computation of PI value
* Parallel (MPI) version (multiple processes, single threads)
*
* @author Dominik Simek
*/


#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <mpi.h>

#ifndef NUM_STEPS
	const unsigned long NSTEPS = 100000000;
#else
	const unsigned long NSTEPS = NUM_STEPS;
#endif

int main(int argc, char *argv[])
{
	MPI_Init(&argc, &argv);

	int my_id      = 0;
	int num_procs  = 0;
	double x       = 0.0;
	double sum     = 0.0;
	double pi      = 0.0;
	double step    = 1.0 / (double)NSTEPS;
	double start_time      = 0.0;
	double wall_time       = 0.0;
	unsigned long my_steps = 0;

	// get my id (rank) and number of processes
	MPI_Comm_rank(MPI_COMM_WORLD, &my_id);
	MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
	my_steps = NUM_STEPS / num_procs;

	// PI = integrate(4.0 / (1+x^2), x=0..1)
	start_time = omp_get_wtime();
	for(unsigned long i = my_id * my_steps; i < (my_id + 1) * my_steps; i++)
	{	
		x = (i - 0.5) * step;
		sum += 4.0 / (1.0 + x * x);
	}
	sum *= step;
	MPI_Reduce(&sum, &pi, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	wall_time = omp_get_wtime() - start_time;

	if(my_id == 0)
	{
		printf("PI = %f (%lu steps of approxiamtion)\n", pi, NSTEPS);
		//printf("Time of computation: %f s (%f ms)\n", wall_time, wall_time * 1000 );
		printf("Time of computation: %.4f s, used %d processes\n", wall_time, num_procs);
	}

	MPI_Finalize();
	return EXIT_SUCCESS;
}