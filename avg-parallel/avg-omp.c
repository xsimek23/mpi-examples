/**
* Parallel computation of average (arrays of numbers)
* Parallel (OpenMP) version (single process, multiple threads)
*
* @author Dominik Simek
*/


#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#ifndef NUM_ELEMENTS
	const unsigned long NELEMENTS = 100000000;
#else
	const unsigned long NELEMENTS = NUM_ELEMENTS;
#endif

/**
* Generate array of "random" values
* (Function always returns same values - better to compare results other versions)
*/
float *get_rand_array(unsigned long nElements)
{
	float *rand_array = (float *)malloc((nElements * sizeof(float)));
	if(!rand_array)
	{
		fprintf(stderr, "Error: malloc()\n");
		return NULL;
	}

	for(unsigned long i = 0; i < nElements; i++)
	{
		rand_array[i] = (rand() / (float)RAND_MAX);
	}

	return rand_array;
} 

/**
* Compute average of values stored in array (numbers)
* Computation is parallel on the threads level
* OpenMP version
*/
float compute_avg(float *numbers, unsigned long elements)
{
	float sum = 0.0f;

	#pragma omp parallel for reduction(+:sum)
	for(unsigned long i = 0; i < elements; i++)
	{
		sum += numbers[i];
	}

	return sum / elements;
}

int main(int argc, char *argv[])
{
	float *global_array    = NULL;
	float global_avg       = 0.0f;
	double start_time      = 0.0;
	double wall_time       = 0.0;
	int num_threads        = 0;

	// allocate array of "random" values
	global_array = get_rand_array(NELEMENTS);
	if(!global_array)
	{
		return EXIT_FAILURE;
	}

	// get number of omp threads
	num_threads = omp_get_max_threads();

	// compute avg (in parallel)
	start_time = omp_get_wtime();
	global_avg = compute_avg(global_array, NELEMENTS);
	wall_time = omp_get_wtime() - start_time;

	printf("Global AVG: %.3f (%lu elements, %d threads)\n", global_avg, NELEMENTS, num_threads);
	printf("Time of computation: %.4f s\n", wall_time);

	// free global memory
	free(global_array);

	return EXIT_SUCCESS;
}