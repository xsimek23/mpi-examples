/**
* Parallel computation of average (arrays of numbers)
* Parallel (MPI) version (multiple processes, single thread)
* (MPI_Scatter, MPI_Reduce)
*
* @author Dominik Simek
*/


#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <mpi.h>

#ifndef NUM_ELEMENTS
	const unsigned long NELEMENTS = 100000000;
#else
	const unsigned long NELEMENTS = NUM_ELEMENTS;
#endif

/**
* Generate array of "random" values
* (Function always returns same values - better to compare results other versions)
*/
float *get_rand_array(unsigned long nElements)
{
	float *rand_array = (float *)malloc((nElements * sizeof(float)));
	if(!rand_array)
	{
		fprintf(stderr, "Error: malloc()\n");
		return NULL;
	}

	for(unsigned long i = 0; i < nElements; i++)
	{
		rand_array[i] = (rand() / (float)RAND_MAX);
	}

	return rand_array;
} 

/**
* Compute average of values stored in array (numbers)
* Computation is sequential on the process level
*/
float compute_avg(float *numbers, unsigned long elements)
{
	float sum = 0.0f;

	for(unsigned long i = 0; i < elements; i++)
	{
		sum += numbers[i];
	}

	return sum / elements;
}

int main(int argc, char *argv[])
{
	MPI_Init(&argc, &argv);

	int my_id      = 0;
	int num_procs  = 0;
	float *local_array     = NULL;
	float *global_array    = NULL;
	float local_avg        = 0.0f;
	float global_avg       = 0.0f;
	float global_avgs_sum  = 0.0f;
	double start_time      = 0.0;
	double wall_time       = 0.0;
	unsigned long elem_per_proc = 0;

	// get my id (rank) and number of processes
	MPI_Comm_rank(MPI_COMM_WORLD, &my_id);
	MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
	
	// calculate number of elements asigned to each process
	elem_per_proc = NELEMENTS / num_procs;

	// master, alocate "random" array
	if(my_id == 0)
	{
		global_array = get_rand_array(NELEMENTS);
		if(!global_array)
		{
			MPI_Finalize();
			return EXIT_FAILURE;
		}
	}

	// allocate local array on each node
	local_array = (float *)malloc(elem_per_proc * sizeof(float));
	if(!local_array)
	{
		fprintf(stderr, "Error: malloc()\n");
		MPI_Finalize();
		return EXIT_FAILURE;
	}

	// transfer data from master to each node
	start_time = omp_get_wtime();
	MPI_Scatter(global_array, elem_per_proc, MPI_FLOAT, local_array, elem_per_proc, MPI_FLOAT, 0, MPI_COMM_WORLD);

	// compute local avg on each node
	local_avg = compute_avg(local_array, elem_per_proc);

	// reduce data from each node to master
	// each node has avg of their local array
	MPI_Reduce(&local_avg, &global_avgs_sum, 1, MPI_FLOAT, MPI_SUM, 0, MPI_COMM_WORLD);
	global_avg = global_avgs_sum / num_procs;
	wall_time = omp_get_wtime() - start_time;

	if(my_id == 0)
	{
		printf("Global AVG: %.3f (%lu elements, %d processes)\n", global_avg, NELEMENTS, num_procs);
		printf("Time of computation: %.4f s\n", wall_time);

		// free global memory
		free(global_array);
	}

	// free local memory
	free(local_array);
	
	MPI_Finalize();
	return EXIT_SUCCESS;
}